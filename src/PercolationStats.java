import java.util.Random;

public class PercolationStats {
private static int n = 2; 
private static int t = 1000;  	

Percolation perc = new Percolation(n); 
double xnum[] = new double[t];  


public PercolationStats(int N, int T){
	n = N; 
	t = T; 
	perc = new Percolation(n); 
	double[] xnum = new double[t];  

}

public static void main(String[] args){  
	
	double[] xnum = new double[t]; 
	int counter = 0; 
	for(int i = 0; i < t; i++){ 
		
		Percolation perc = new Percolation(n);
		while(!perc.percolates()){ 
			perc.open(StdRandom.uniform(0, n), StdRandom.uniform(0, n));
			counter++; 
		}
		xnum[i] = counter/(n*n);
	}
	
	System.out.println("Java PercolationStats"+n*n+" "+ t);
	System.out.println("mean                    = " + mean(xnum,t));
	System.out.println("stddev                  = "+ stddev(xnum, mean(xnum,t), t)); 
	System.out.println("95% confidence interval = " + confidenceLo(mean(xnum,t), stddev(xnum, mean(xnum,t),t), t)+","+confidenceHi(mean(xnum,t), stddev(xnum, mean(xnum,t),t), t));
}

public static double mean(double[] x, double t){
	double sum = 0; 
		for(int i = 0; i < x.length;i++){ 
			sum += x[i]; 
		}
		return sum/t; 
	}
 
public static double stddev(double[] x, double mean, double t){ 
	double xminmeansq = 0; 
	for(int i = 0; i < t; i++){ 
		 
		xminmeansq += Math.pow(x[i] - mean,2); 
	}
	return Math.sqrt(xminmeansq/(t-1)); 
}

public static double confidenceLo(double mean, double stddev, double t){ 
	return mean - ((stddev*1.96)/Math.sqrt(t)); 
}

public static double confidenceHi(double mean, double stddev, double t){ 
	return mean + ((stddev*1.96)/Math.sqrt(t));
}

}