public class Percolation {
  private int num ;
  private UF uf; 
  private int[] grid; 
  private int TOP_NODE; 
  private int BOT_NODE; 
  private int opened; 
// 0 = open, 1 = closed
  public Percolation(int N){ 
	 
	  num = N; 
	  grid = new int[(N*N)+2]; 
	  
	  uf = new UF((N*N)+2); 
	 
	  TOP_NODE = grid.length -2; 
	  BOT_NODE = grid.length -1; 
	  initializeGrid(grid, N); 
	  opened = 0; 
  } 
  
  
  public int posIn1d(int row, int col){
	  return (num*(row-1))+ (col -1);
  }
  
 
  
  public void initializeGrid(int[] grid, int N){
    for(int i = 0; i < grid.length - 2; i++){ 
    	grid[i] = 1; 
    }
    grid[0] = 1; 
    grid[TOP_NODE] = 0; 
    grid[BOT_NODE] = 0; 
    
    for(int i = 0; i < num; i++){ 
    	uf.union(i, TOP_NODE);
    	System.out.println(i+" should be connected to "+TOP_NODE); 
    }
    for(int i = 0; i < num; i++){ 
    	uf.union(posIn1d(num,i),BOT_NODE); 
    	System.out.println(posIn1d(num,i)+" should be connected to "+BOT_NODE); 
    }
    
  }
  
  
  
  public boolean isFull(int row, int col){ 
	 return(isOpen(row,col) && uf.connected(posIn1d(row,col), TOP_NODE)); }
  
  
  public boolean isOpen(int row, int col){ 
	return(grid[posIn1d(row,col)] == 0); }
  
  
  
  public boolean percolates(){ 
	  return(uf.connected(TOP_NODE, BOT_NODE)); }
  
  
  public int numOpen(){ 
	  return opened; 
  }
  
  
  public void open(int row, int col){
	  opened++; 
	  grid[posIn1d(row,col)] = 0; 
	  System.out.println(grid[posIn1d(row,col)]);
	  
	  int base = posIn1d(row,col); 
	  int l = posIn1d(row,col-1); 
	  int r = posIn1d(row,col+1);
	  int u = posIn1d(row-1, col); 
	  int d = posIn1d(row+1, col);
	  
	  
	  
	  
	 
	  //topleft
	  if(row == 1 && col == 1){ 

		  
		  boolean ro = isOpen(row,col+1);
		
		  boolean od = isOpen(row+1, col);
		  if(ro){
			  uf.union(r, base);
			  System.out.println("Connected right");
		  }
		  if(od){
			  uf.union(d, base); 
			  System.out.println("Connected down");
		  }
	  }
	  //topright
	  else if(row == 1 && col == num){ 

		  boolean lo = isOpen(row,col-1); 
		 
		  boolean od = isOpen(row+1, col);
		  
		  if(od){
			  uf.union(d, base); 
			  System.out.println("Connected down");
		  }
		  if(lo){ 
			  uf.union(l,base); 
			  System.out.println("Connected left");
		  }
	  }
	  //bottomleft
	  else if(row == num && col == 1){ 
 
		  boolean ro = isOpen(row,col+1);
		  boolean uo = isOpen(row-1, col); 
		 
		  
		  if(ro){
			  uf.union(r, base);
			  System.out.println("Connected right");
		  }
		  if(uo){
			  uf.union(u, base); 
			  System.out.println("Connected up");
		  }
	  }
	  
	  //bottomright
	  else if(row == num && col == num){ 

		  boolean lo = isOpen(row,col-1); 
		 
		  boolean uo = isOpen(row-1, col); 
		
		  
		  if(lo){ 
			  uf.union(l,base); 
			  System.out.println("Connected left");
		  }
		  
		  if(uo){
			  uf.union(u, base); 
			  System.out.println("Connected up");
		  }
		  
	  }
	  
	  else if(row == 1 && col > 1 && col < num){

		  boolean lo = isOpen(row,col-1); 
		  boolean ro = isOpen(row,col+1);
		  
		  boolean od = isOpen(row+1, col);
		  
		  if(lo){ 
			  uf.union(l,base); 
			  System.out.println("Connected left");
		  }
		  if(ro){
			  uf.union(r, base);
			  System.out.println("Connected right");
		  }
		  
		  if(od){
			  uf.union(d, base); 
			  System.out.println("Connected down");
		  }
		  
	  }
	  
	  else if(row == num && col > 1 && col < num){ 

		  boolean lo = isOpen(row,col-1); 
		  boolean ro = isOpen(row,col+1);
		  boolean uo = isOpen(row-1, col); 
		  
		  
		  if(lo){ 
			  uf.union(l,base); 
			  System.out.println("Connected left");
		  }
		  if(ro){
			  uf.union(r, base);
			  System.out.println("Connected right");
		  }
		  if(uo){
			  uf.union(u, base); 
			  System.out.println("Connected up");
		  }
	  }
	  
	  else if(col == 1 && row > 1 && row < num){ 

		  
		  boolean ro = isOpen(row,col+1);
		  boolean uo = isOpen(row-1, col); 
		  boolean od = isOpen(row+1, col);
		  
		  if(ro){
			  uf.union(r, base);
			  System.out.println("Connected right");
		  }
		  if(uo){
			  uf.union(u, base); 
			  System.out.println("Connected up");
		  }
		  if(od){
			  uf.union(d, base); 
			  System.out.println("Connected down");
		  }
	  }
	  
	  else if(col == num && row > 1 && row < num){ 

		  boolean lo = isOpen(row,col-1); 
		  
		  boolean uo = isOpen(row-1, col); 
		  boolean od = isOpen(row+1, col);
		  
		  if(uo){
			  uf.union(u, base); 
			  System.out.println("Connected up");
		  }
		  if(od){
			  uf.union(d, base); 
			  System.out.println("Connected down");
		  }
		  if(lo){ 
			  uf.union(l,base); 
			  System.out.println("Connected left");
		  }
	  }
	  
	  //free
	  else{ 

		  boolean lo = isOpen(row,col-1); 
		  boolean ro = isOpen(row,col+1);
		  boolean uo = isOpen(row-1, col); 
		  boolean od = isOpen(row+1, col);
		  
		  if(lo){ 
			  uf.union(l,base); 
			  System.out.println("Connected left");
		  }
		  if(ro){
			  uf.union(r, base);
			  System.out.println("Connected right");
		  }
		  if(uo){
			  uf.union(u, base); 
			  System.out.println("Connected up");
		  }
		  if(od){
			  uf.union(d, base); 
			  System.out.println("Connected down");
		  }
	  }
	  
	  
	  
	  
	  
  }
 
}
